# Instructions

  1. Clone repo
  2. Download `http-server` from NPMJS (https://www.npmjs.com/package/http-server), or from an adminstrative shell/terminal/bash session, run `npm install --global http-server`
  3. change into newly cloned directory, and run `npm install` (you will get warnings - these are okay)
  4. From the root of the directory, run `http-server .` (NB the period '.' at the end)
  5. Browse to `http://127.0.0.1:8080`, and then click on the *Forms* link