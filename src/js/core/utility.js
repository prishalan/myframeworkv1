var utility = {

	// === TOGGLE ACTIVE/FOCUS STATES ON INPUT TEXT FIELDS
	textFieldClassToggle: function () {
		this.parentElement.parentElement.classList.toggle('has-focus');
	},

	// === TOGGLE ACTIVE/FOCUS STATES ON SELECT FIELDS
    selectFieldClassToggle: function () {
        this.parentElement.parentElement.parentElement.classList.toggle('has-focus');
    },

	// === TOGGLE ACTIVE/FOCUS STATES ON SELECT INPUT SEARCH FIELDS
	selectSearchFieldClassAdd: function() {
		this.parentElement.parentElement.parentElement.parentElement.parentElement.classList.add('search-has-focus');
	},
	selectSearchFieldClassRemove: function() {
        this.parentElement.parentElement.parentElement.parentElement.parentElement.classList.remove('search-has-focus');
    },

	// === CREATE A DOM ELEMENT
	createElement: function(e, a) {
		var elem = document.createElement(e);
		if (a && Object.prototype.toString.call(a) === "[object Object]") {
			for (var i in a) {
				if (i in elem) {
					elem[i] = a[i];
				}
				else if (i === 'html') {
					elem.innerHTML = a[i];
				}
				else if (i === 'text') {
					elem.appendChild(document.createTextNode(a[i]));
				}
				else {
					elem.setAttribute(i, a[i]);
				}
			}
		}
		return elem;
	},

	// === ADD AN INPUT MASK
	createInputMask: function(elem, mask) {

	}

};
