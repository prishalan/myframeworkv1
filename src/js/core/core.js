document.addEventListener('DOMContentLoaded', function() {

	// Toggle focus/blur states on form text inputs
	[].forEach.call(document.querySelectorAll('input:not([type=radio]):not([type=checkbox])'), function(el) {
		el.addEventListener('focus', utility.textFieldClassToggle, true);
		el.addEventListener('blur', utility.textFieldClassToggle, true);
	});

	// Toggle focus/blur states on form select fields
	[].forEach.call(document.querySelectorAll('select'), function(el) {
		new Selectr(el, {
			defaultSelected: false,
            searchable: eval(el.dataset.searchable),
            multiple: eval(el.dataset.multiple)
		});
        el.previousSibling.previousSibling.addEventListener('focus', utility.selectFieldClassToggle, true);
		el.previousSibling.previousSibling.addEventListener('blur', utility.selectFieldClassToggle, true);
	});

	// Toggle focus/blur states on select text search inputs
	[].forEach.call(document.querySelectorAll('input.selectr-input'), function(el) {
        el.addEventListener('focus', utility.selectSearchFieldClassAdd, true);
        el.addEventListener('blur', utility.selectSearchFieldClassRemove, true);
    });

	// Add UI enhancement to Range Slider
	// [].forEach.call(document.querySelectorAll('input[type="range"]'), function (el) {
		// new JSR(el);
	new JSR(['#jsr-1-1', '#jsr-1-2'], {
		sliders: 2,
		values: [25, 50],
		grid: false
	});
	// });


	// Input Masks
	// var phoneMask = ['(', /[0]/, /[1-9]/, /[0-9]/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
	var phoneMask = ['/\d{1,3}(?=(\d{3})+(?!\d))/'];
	var myInput = document.getElementById('field1');
	var maskedInputController = vanillaTextMask.maskInput({
		inputElement: myInput,
		mask: phoneMask
	});

	// Datepicker
	document.getElementById('date1').DatePickerX.init({
		mondayFirst: false,
		format: 'yyyy-mm-dd',
	});


	var jsr1 = document.querySelector("[data-range='to']");
	console.log(jsr1.id);

	document.getElementById('testbutton').addEventListener('click', function() {
		console.dir(document.getElementById('field1'))
	});

});

