'use strict';

var gulp 	= require('gulp'),
	sass    = require('gulp-sass'),
	uglify  = require('gulp-uglify'),
	concat 	= require('gulp-concat'),
	batch	= require('gulp-batch'),
	watch   = require('gulp-watch');


// -------------------------------------------------------- Transpile sass
gulp.task('sass', function () {
	return gulp.src('./src/scss/*.scss')
		.pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
		.pipe(gulp.dest('./public/css'));
});


// -------------------------------------------------------- Combine all JS files
gulp.task('js-core', function () {
	gulp.src([
		'./src/js/vendor/bind.js',
		'./src/js/vendor/DatePickerX.js',
		'./src/js/vendor/jsr.js',
		'./src/js/vendor/tingle.js',
		'./src/js/vendor/vanillaTextMask.js',
		'./src/js/vendor/js-form-validator.js',
		'./src/js/vendor/selectr.js',
		'./src/js/vendor/enquire.js',
		'./src/js/core/utility.js',
		'./src/js/core/core.js'
	])
		.pipe(concat('core.js'))
		.pipe(uglify())
		.pipe(gulp.dest('./public/js'));
});
// gulp.task('js-auth', function () {
// 	gulp.src('./src/js/auth/**/*.js')
// 		.pipe(concat('auth.js'))
// 		.pipe(uglify())
// 		.pipe(gulp.dest('./public/js'));
// });
// gulp.task('js-app', function () {
// 	gulp.src('./src/js/app/**/*.js')
// 		.pipe(concat('app.js'))
// 		.pipe(uglify())
// 		.pipe(gulp.dest('./public/js'));
// });


// -------------------------------------------------------- Gulp Watch tasks
gulp.task('watch', function () {
	watch('./src/scss/**/*.scss', batch(function (events, done) {
		gulp.start('sass', done);
	}));
	watch('./src/js/core/*.js', batch(function (events, done) {
		gulp.start('js-core', done);
	}));
	watch('./src/js/auth/*.js', batch(function (events, done) {
		gulp.start('js-auth', done);
	}));
	watch('./src/js/app/*.js', batch(function (events, done) {
		gulp.start('js-app', done);
	}));
});
